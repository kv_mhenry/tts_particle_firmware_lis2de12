/**
 *****************************************************************************
 * @file     lis2de12.h 
 * @brief    lis2de12.h software unit interface and data structure declarations.
 * @author   Parker Kamer
******************************************************************************
 *
 * @copyright COPYRIGHT (c) 2020 SAEC Kinetic Vision, Inc.
 *            All Rights Reserved.
 *
 * This software is properietary of SAEC Kinetic Vision, Inc., Inc and is
 * confidential. Unauthorized use, publication, disclosure, possession, 
 * or duplication exposes you to severe civil and/or criminal penalty. 
 *
 ******************************************************************************
 * 
 * Significant Modification History (Most Recent at top)
 *
 * Date        | Initials | Description
 * ----------- | -------- | -----------
 * 06 FEB 2020 | PK       | Original
 */
// Includes ------------------------------------------------------------------
#include "Particle.h"
#include <math.h>
#include <limits.h>
#include "lis2de12.h"

// Private constants ---------------------------------------------------------
#define DEFAULT_LIS2DE12_FREQ 10

// Private types -------------------------------------------------------------

// Private variables ---------------------------------------------------------

// Private macros ------------------------------------------------------------

// Private types -------------------------------------------------------------

// Private function bodies ---------------------------------------------------

///
/// Writes an 8 bit value over I2C
///

bool LIS2DE12::write8(lis2de12_reg_t reg, byte value) {
    Wire.beginTransmission(_address);
    
    Wire.write((uint8_t)reg);
    Wire.write((uint8_t)value);
    
    Wire.endTransmission();
    
    /* ToDo: Check for error! */
    return true;
}

///
/// Reads an 8 bit value over I2C
///

byte LIS2DE12::read8(lis2de12_reg_t reg ) {
    byte value = 0;
    
    Wire.beginTransmission(_address);
    Wire.write((uint8_t)reg);
    Wire.endTransmission();
    Wire.requestFrom(_address, (byte)1);
    value = Wire.read();
    
    return value;
}

///
/// Reads the specified number of bytes over I2C
///

bool LIS2DE12::readLen(lis2de12_reg_t reg, byte * buffer, uint8_t len) {
    Wire.beginTransmission(_address);
    Wire.write((uint8_t)reg);
    Wire.endTransmission();
    Wire.requestFrom(_address, (byte)len);
    
    for (uint8_t i = 0; i < len; i++) {
        buffer[i] = Wire.read();
    }
    
    /* ToDo: Check for errors! */
    return true;
}



// Public function ----------------------------------------------------------
LIS2DE12::LIS2DE12(uint8_t address, pin_t pin1) { 
    _address = address;

   pinMode(pin1, INPUT);
   attachInterrupt(pin1, &LIS2DE12::interruptHandler, this, CHANGE);
}

void LIS2DE12::interruptHandler() {
    pinMode(D8, INPUT);
   _Movement = 1;
    _newState = pinReadFast(D8);
    if (_lastState != _newState){
        _Movement = 2;
    }
        
}        


bool LIS2DE12::begin(void) {

    /*Device start up time*/
    delay(6);

    /*Who am i*/
    
    uint8_t deviceId = read8(whoAmI);
    if(deviceId != LSI2DE12_CHIP_ID_VAL){
        Serial.println("NO LIS2DE12");
        Serial.println(deviceId);
        delay(3000);
    }
    else{
    Serial.println("LIS2DE12 FOUND");
            
    }

    //Device Setup    
    write8(ctrlReg0, 0x10);
    write8(ctrlReg1, 0x2F);
    
    write8(ctrlReg5, 0x40);
    write8(fifoCtrlReg, 0x80);

    //Sleep to Wake
    write8(actThs, 7); // 112mg ( 1 LSb = 16 mg @ FS = ±2 g )

    uint8_t lsbValue;
    uint16_t noMotionSleppTime = 5;
    uint8_t odr = 10; //Make sure this odr matches ctrlReg1
	if(0 < noMotionSleppTime){
		lsbValue = ((noMotionSleppTime * odr)/8)-1;
        
	}
	else{
		lsbValue = 0;
	}
    write8(actDur, lsbValue);// seconds ( (8*LSB+1)/ODR )
    
    write8(ctrlReg6, 0x0A); //Might be what starts interrupt

    _FreqHz = DEFAULT_LIS2DE12_FREQ;

    return true;
}

bool LIS2DE12::getAccelData(float* data) {

	uint16_t aData[3];

    readLen( fifoReadStart, (byte*)&aData, 6);

	data[0] = ((float)((int8_t)(aData[0] / 256.0f))) * 16.0f;
	data[1] = ((float)((int8_t)(aData[1] / 256.0f))) * 16.0f;
	data[2] = ((float)((int8_t)(aData[2] / 256.0f))) * 16.0f;

    return true;
}

void LIS2DE12::tick(void) {
    uint32_t currentTime = millis();
    
    if(currentTime - _LastUpdateRunTime > (1000/_FreqHz)){
        getAccelData(accelData);
        _LastUpdateRunTime = currentTime;
    }
}