/**
 ******************************************************************************
 * @file    tts-particle.ino
 * @author  Stanley Huang
 ******************************************************************************
 *
 * @copyright COPYRIGHT (c) 2019 SAEC Kinetic Vision, Inc 
 *            All Rights Reserved
 *
 * This software is property of SAEC Kinetic Vision, Inc and is considered
 * confidential.
 *
 ******************************************************************************
 *
 * Significant Modification History (Most Recent at top)
 * -----------------------------------------------------
 *
 * Date        | Initials | Description
 * ----------- | -------- | -----------
 * 01 DEC 2019 |   SH     | Original
 *
 * Theory of Operation
 * -------------------
 * TBD
 *
 *
 *
 */

// Includes ------------------------------------------------------------------

#include <Particle.h>

#if ARDUINO >= 100
    #include "Arduino.h"
#else
    #include "WProgram.h"
#endif

#include <stdlib.h>
#include "HX711ADC.h"
#include "encoder.h"
#include "rfid_mfrc522.h"
#include "rfid_pn532.h"
#include "Sd.h"
#include "BME280.h"
#include <stdio.h>
#include "Bno055.h"
#include "lis2de12.h"

#include "dct.h"
SYSTEM_MODE(SEMI_AUTOMATIC);

// Boron Product Info ---------------------------------------------------

PRODUCT_ID(10548);      //TTS Boron project key
PRODUCT_VERSION(2);     //TTS Boron firmware version

// Pin Definition -------------------------------------------------------------

#define HX711_DOUT_PIN      D3
#define HX711_SCK_PIN       D2

#define ENCODER_PIN_A       D4
#define ENCODER_PIN_B       D5
#define INT2                D8
#define SD_SS_PIN           A5

#define PN532_IRQ_PIN       D6
#define PN532_RST_PIN       D7

#define LI2DE12_IRQ_PIN     D8

// Constants -----------------------------------------------------------------

//#define GLOBAL_DEBUG

#define SERIAL_DATA_LINE

// N/A      = 0
// Bno055   = 1
// Lis2de12 = 2
#define IMU_DATA 2

#define DEFAULT_WRITE_SD_FREQUENCY 100

#define SEALEVELPRESSURE_HPA (1013.25)

#define BNO055_SAMPLERATE_DELAY_MS (100)

// Private types -------------------------------------------------------------

HX711ADC _Hx711(HX711_DOUT_PIN, HX711_SCK_PIN); 

Encoder _Encoder(ENCODER_PIN_A, ENCODER_PIN_B);

Pn532 _RfidPn532(PN532_IRQ_PIN, PN532_RST_PIN);

Sd _SdReader;

Adafruit_BME280 _Bme280;

Adafruit_BNO055 _Bno055 = Adafruit_BNO055(55, 0x28);

LIS2DE12 _Lis2de12 = LIS2DE12(0x19, LI2DE12_IRQ_PIN);

// Private variables ---------------------------------------------------------

uint32_t _LastPublishTime = 0;

uint16_t _WriteSdFrequency = DEFAULT_WRITE_SD_FREQUENCY;

uint32_t _LastUpdatingRunTime = 0;

double _PositionF;

uint32_t _Epoch;

uint32_t _EpochLast;

uint32_t _MillisLast;

uint32_t _Millis;

uint32_t _MillisOffset;

uint32_t _CurrentTime;

float _AccelDataLis[3];

int val =0;

float _AccelData[3];
float _GyroData[3];
float _MagData[3];
float _EulerData[3];
float _QuatData[4];

// Public variables --------------------------------------------------------

int32_t _LoadCellWeight = 0;

int32_t _Position = 0;

float _Temperature = 0;

float _Humidity = 0;

String _RFID = "";

char _Buffer[50];

bool _Moving;

// Setup function bodies ---------------------------------------------------

int getSdFreq(String frequency);

// Loop functions bodies ---------------------------------------------------

void setup() {

    Serial.begin(115200);
    
    //I2C Init
    Wire.begin();
    Wire.setSpeed(CLOCK_SPEED_100KHZ);

    _RfidPn532.begin();
    _Hx711.begin();
    _Bme280.begin();
    _Lis2de12.begin();
    _Bno055.begin();    
    
    Time.zone(-5);
    
    _SdReader.begin();
   
    delay(1000);
    
    /* Display some basic information on this sensor */
    Bno055_DisplaySensorDetails();
    
    /* Optional: Display current status */
    Bno055_DisplaySensorStatus();
    
    _Bno055.setExtCrystalUse(true);

    Particle.variable("RFID", _RFID);
    Particle.variable("Weight", _LoadCellWeight);
    Particle.function("SD_write_frequency", getSdFreq);

    
    _Epoch = _EpochLast = Time.now();
    while(_Epoch == _EpochLast){
        _EpochLast = _Epoch;
        _Epoch = Time.now();
    }
    _MillisLast = millis();

    _Moving = TRUE;
    Serial.println("Device displaying data: ");
}

void loop() {

    _Hx711.tick();
    _RfidPn532.tick();
    _Bno055.tick();
    _Lis2de12.tick();

    if (_Moving==TRUE){
        writeValues();
    }

    //writeValues();
    //Serial.println(_Lis2de12.int2Src);

    //Serial.print("Last State = ");
    //Serial.print(_Lis2de12._lastState);
    //Serial.print (   "New State = ");
    //Serial.println(_Lis2de12._newState);
    //Serial.print("  Movement = ");
    //Serial.println(_Lis2de12._Movement);
    /*if((_Lis2de12._Movement == true ) && (_Lis2de12._StateChange == true)){
        Serial.println("The Device is Moving ");
    }
    if((_Lis2de12._Movement == false ) && (_Lis2de12._StateChange == false)){
        Serial.println("The Device is not Moving");
    }*/
 
    if((_Lis2de12._Movement == 2) && (_Lis2de12._newState ==1)){
        Serial.println("The device is moving.");
        _Lis2de12._Movement = 1;
        _Moving = TRUE;
    }

    if((_Lis2de12._Movement == 1) && (_Lis2de12._newState ==0)){
        Serial.println("The device is not moving.");
        _Lis2de12._newState = 0;
        _Lis2de12._Movement =0;
        _Moving = FALSE;
    }
    
    if (_Moving==TRUE){
        writeValues();
    }

    delay (250);


}

void writeValues(){
    //uint32_t currentTime = millis();
    _CurrentTime = millis();
    
    //if(currentTime - _LastPublishTime > (1000/_WriteSdFrequency)){
    if(_CurrentTime - _LastPublishTime > (1000/_WriteSdFrequency)){
        String data;
        
        updateValues();
        
        constructString(&data);
    
        bool success = _SdReader.write(data);
       
      
#ifdef SERIAL_DATA_LINE
        Serial.println(data);
#endif
        
        #ifdef GLOBAL_DEBUG
            
            if(success){
                //Serial.print("Write SD Success");   
            } else {
                //Serial.print("Write failed");
            }
            
                //  Serial.print(event.timestamp, 4);
    
            /* Display the floating point data */
            Serial.print("Orientation ->");
            Serial.print("X: ");
            Serial.print(_SensorEvent.orientation.x, 4);
            Serial.print("\tY: ");
            Serial.print(_SensorEvent.orientation.y, 4);
            Serial.print("\tZ: ");
            Serial.println(_SensorEvent.orientation.z, 4);
            
            Serial.print("\tGyroscope ->");
            Serial.print("\tX: ");
            Serial.print(_SensorEvent.gyro.x, 4);
            Serial.print("\tY: ");
            Serial.print(_SensorEvent.gyro.y, 4);
            Serial.print("\tZ: ");
            Serial.println(_SensorEvent.gyro.z, 4);
            
            Serial.print("\tAcceleration ->");
            Serial.print("\tX: ");
            Serial.print(_SensorEvent.acceleration.x, 4);
            Serial.print("\tY: ");
            Serial.print(_SensorEvent.acceleration.y, 4);
            Serial.print("\tZ: ");
            Serial.println(_SensorEvent.acceleration.z, 4);
        
            Serial.print("Magnetic ->");
            Serial.print("\tX: ");
            Serial.print(_SensorEvent.magnetic.x, 4);
            Serial.print("\tY: ");
            Serial.print(_SensorEvent.magnetic.y, 4);
            Serial.print("\tZ: ");
            Serial.println(_SensorEvent.magnetic.z, 4);
            
            //Serial.print("\t");

            /*Serial.print(Time.format(_Epoch));
            Serial.print("\t");
                        
            Serial.print(_LoadCellWeight);
            Serial.print("\t");
            
            Serial.print(_Position);
            Serial.print("\t");
  
            Serial.print(_PositionF);
            Serial.print("\t");
  
            Serial.print(_Temperature);
            Serial.print("\t");
            
            Serial.print(_Humidity);
            Serial.print("\t");
  
            Serial.print(_RFID);
            Serial.print("\t");
            
            Serial.println();*/
        
        #endif
        
        //_LastPublishTime = currentTime;
        _LastPublishTime = _CurrentTime;
    }
}

int getSdFreq(String frequency){
    int writeFrequency = atoi (frequency);
    if(writeFrequency){
        _WriteSdFrequency = writeFrequency;
        return 1;
    } 
    return 0;
}


void updateValues(){
    
    _LoadCellWeight = _Hx711.getWeight();
    
    _Position = _Encoder.getPosition();
    //_Position = _Position / 2.00 / 24.00;
    _PositionF = (float)_Position / 96.00;/// 2.00 / 24.00;
    _RFID = _RfidPn532.getCardId();
    
    _Temperature = _Bme280.readTemperature();
    
    _Humidity = _Bme280.readHumidity();
    
    /*IMU Data*/
#if (IMU_DATA == 1)
    _Bno055.getSensorData(_AccelData, SENSOR_TYPE_ACCELEROMETER);
    _Bno055.getSensorData(_GyroData, SENSOR_TYPE_GYROSCOPE);
    _Bno055.getSensorData(_MagData, SENSOR_TYPE_MAGNETIC_FIELD);
#elif (IMU_DATA == 2)
    _AccelDataLis[0] = _Lis2de12.accelData[0];
    _AccelDataLis[1] = _Lis2de12.accelData[1];
    _AccelDataLis[2] = _Lis2de12.accelData[2];
#endif

    _EpochLast = _Epoch;
    _Epoch = Time.now();
    
    _Millis += _CurrentTime - _MillisLast;
    
    if(_Millis >= 1000){
        _Millis -= 1000;
    }
    else if(_Epoch != _EpochLast){
       _Millis = 0;
    }
    _MillisLast = _CurrentTime;
}

void constructString(String* stringData){
    String data;
    
    //Sat Jan 10 08:22:04 2004
    data += Time.format(_Epoch, "%D %T.");
    sprintf(_Buffer, "%lu, ", _Millis);
    data += _Buffer;
    
    //sprintf(_Buffer, "%d \t %d \t %4.1f \t %4.1f \t", _LoadCellWeight, _Position, _Temperature, _Humidity);
    //sprintf(_Buffer, "%d,%.2f,%4.1f,%4.1f,", _LoadCellWeight, _PositionF, _Temperature, _Humidity);
    sprintf(_Buffer, "%d,%d,%.2f,%4.1f,%4.1f,", _LoadCellWeight, _Position, _PositionF, _Temperature, _Humidity);
    data += _Buffer;

#if (IMU_DATA == 1)
    sprintf(_Buffer, "%f,%f,%f,", _AccelData[0], _AccelData[1], _AccelData[2]);
    data += _Buffer;
    
    sprintf(_Buffer, "%f,%f,%f,", _GyroData[0], _GyroData[1], _GyroData[2]);
    data += _Buffer;
    
    sprintf(_Buffer, "%f,%f,%f,", _MagData[0], _MagData[1], _MagData[2]);
    data += _Buffer;
#elif (IMU_DATA == 2)
    sprintf(_Buffer, "%f,%f,%f,", _AccelDataLis[0], _AccelDataLis[1], _AccelDataLis[2]);
    data += _Buffer;
#endif

    data += _RFID;
    data += "\t";
    
    *stringData = data;
}


///
///    Displays some basic information on this sensor from the unified
///    sensor API sensor_t type (see Adafruit_Sensor for more information)
///

void Bno055_DisplaySensorDetails(void) {
    sensor_t sensor;
    _Bno055.getSensor(&sensor);
    
    Serial.println("------------------------------------");
    Serial.print  ("Sensor:       "); Serial.println(sensor.name);
    Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
    Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
    Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" xxx");
    Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" xxx");
    Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" xxx");
    Serial.println("------------------------------------");
    Serial.println("");
    delay(500);
}

///
///Display some basic info about the sensor status
///

void Bno055_DisplaySensorStatus(void) {
    /* Get the system status values (mostly for debugging purposes) */
    uint8_t system_status, self_test_results, system_error;
    system_status = self_test_results = system_error = 0;
    _Bno055.getSystemStatus(&system_status, &self_test_results, &system_error);
    
    /* Display the results in the Serial Monitor */
    Serial.println("");
    Serial.print("System Status: 0x");
    Serial.println(system_status, HEX);
    Serial.print("Self Test:     0x");
    Serial.println(self_test_results, HEX);
    Serial.print("System Error:  0x");
    Serial.println(system_error, HEX);
    Serial.println("");
    delay(500);
}

///
///Display sensor calibration status
/// 

void Bno055_DisplayCalStatus(void) {
    /* Get the four calibration values (0..3) */
    /* Any sensor data reporting 0 should be ignored, */
    /* 3 means 'fully calibrated" */
    uint8_t system, gyro, accel, mag;
    system = gyro = accel = mag = 0;
    _Bno055.getCalibration(&system, &gyro, &accel, &mag);
    
    /* The data should be ignored until the system calibration is > 0 */
    Serial.print("\t");
    
    // if (!system) {
    //     Serial.print("! ");
    // }
    
    /* Display the individual values */
    // Serial.print("Sys:");
    // Serial.print(system, DEC);
    // Serial.print(" G:");
    // Serial.print(gyro, DEC);
    // Serial.print(" A:");
    // Serial.print(accel, DEC);
    // Serial.print(" M:");
    // Serial.print(mag, DEC);
}

// void printEvent(sensors_event_t* event) {
//     Serial.println();
//     Serial.print(event->type);
//     double x = -1000000, y = -1000000 , z = -1000000; //dumb values, easy to spot problem
//     if (event->type == SENSOR_TYPE_ACCELEROMETER) {
//         x = event->acceleration.x;
//         y = event->acceleration.y;
//         z = event->acceleration.z;
//     }
//     else if (event->type == SENSOR_TYPE_ORIENTATION) {
//         x = event->orientation.x;
//         y = event->orientation.y;
//         z = event->orientation.z;
//     }
//     else if (event->type == SENSOR_TYPE_MAGNETIC_FIELD) {
//         x = event->magnetic.x;
//         y = event->magnetic.y;
//         z = event->magnetic.z;
//     }
//     else if ((event->type == SENSOR_TYPE_GYROSCOPE) || (event->type == SENSOR_TYPE_ROTATION_VECTOR)) {
//         x = event->gyro.x;
//         y = event->gyro.y;
//         z = event->gyro.z;
//     }
    
//     Serial.print(": x= ");
//     Serial.print(x);
//     Serial.print(" | y= ");
//     Serial.print(y);
//     Serial.print(" | z= ");
//     Serial.println(z);
// }

// void printValues(){
//     uint32_t currentTime = millis();
//     if(currentTime - _LastUpdatingRunTime > (1000/(10))){
    
//         Serial.print("Temperature = ");
//         Serial.print(_Bme280.readTemperature());
//         Serial.println(" *C");
    
//         Serial.print("Pressure = ");
    
//         Serial.print(_Bme280.readPressure() / 100.0F);
//         Serial.println(" hPa");
    
//         Serial.print("Approx. Altitude = ");
//         Serial.print(_Bme280.readAltitude(SEALEVELPRESSURE_HPA));
//         Serial.println(" m");
    
//         Serial.print("Humidity = ");
//         Serial.print(_Bme280.readHumidity());
//         Serial.println(" %");
    
//         Serial.println();
        
//         _LastUpdatingRunTime = currentTime;
//     }
// }




