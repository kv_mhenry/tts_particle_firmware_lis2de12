/**
 *****************************************************************************
 * @file     lis2de12.h 
 * @brief    lis2de12.h software unit interface and data structure declarations.
 * @author   MH and PK 
******************************************************************************
 *
 * @copyright COPYRIGHT (c) 2020 SAEC Kinetic Vision, Inc.
 *            All Rights Reserved.
 *
 * This software is properietary of SAEC Kinetic Vision, Inc., Inc and is
 * confidential. Unauthorized use, publication, disclosure, possession, 
 * or duplication exposes you to severe civil and/or criminal penalty. 
 *
 ******************************************************************************
 * 
 * Significant Modification History (Most Recent at top)
 *
 * Date        | Initials | Description
 * ----------- | -------- | -----------
 * 06 FEB 2020 | PK       | Original
 */
// Define to prevent recursive inclusion -------------------------------------
#ifndef __LIS2DE12_H
#define __LIS2DE12_H
using namespace std;

// Exported macro ------------------------------------------------------------

// Exported types ------------------------------------------------------------

// Exported constants --------------------------------------------------------
#define LSI2DE12_I2C_ADDRESS 0x19 //0x32
#define LSI2DE12_CHIP_ID_VAL 0x33

// Exported objects ----------------------------------------------------------
class LIS2DE12
{
    public:
      typedef enum
      {
        	whoAmI = 0x0F,
          ctrlReg0 = 0x1E,
          ctrlReg1 = 0x20,
          ctrlReg2,
          ctrlReg3,
          ctrlReg4,
          ctrlReg5,
          ctrlReg6,
          outX = 0x29,
          reserved1,
          outY,
          reserved2,
          outZ,
          fifoCtrlReg,
          int2Cfg = 0x34,
          int2Src,
          int2Ths,
          int2Duration,
          actThs = 0x3E,
          actDur,
          
          fifoReadStart = 0xA8,
      }lis2de12_reg_t;

      LIS2DE12 ( uint8_t address = LSI2DE12_I2C_ADDRESS, pin_t pin1 = D8);

      bool begin(void);
      bool getAccelData(float* data);

      /*Updates sensor data*/
      void tick(void);
     
      float accelData[3];
      //Interupt Handler Variables
      
      bool _lastState;
      uint8_t _newState;
      
      uint8_t _Movement = 0;
      
      

    private:
      byte  read8   ( lis2de12_reg_t );
      bool  readLen ( lis2de12_reg_t, byte* buffer, uint8_t len );
      bool  write8  ( lis2de12_reg_t, byte value );
      void interruptHandler();

         
      
      uint16_t _FreqHz;
      uint32_t _LastUpdateRunTime;

      uint8_t _address;
};

// Exported functions --------------------------------------------------------

#endif // __LIS2DE12_H

/// SAEC Kinetic Vision, Inc. ----------- END OF FILE




